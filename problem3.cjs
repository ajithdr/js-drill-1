/*  ==== Problem #3 ====
    The marketing team wants the car models listed alphabetically on the website. 
    Execute a function to Sort all the car model names into alphabetical order and 
    log the results in the console as it was returned.
*/

const inventory = require('./inventory.cjs');

function problem3(inventory) {

    if(arguments.length < 1 || !Array.isArray(inventory)) {
        return [];
    }

    const carModels = [];

    for(let index = 0; index < inventory.length; index++) {
        if(inventory[index].hasOwnProperty('car_model')) {
            carModels.push(inventory[index].car_model)
        }
    }

    carModels.sort();
    return carModels;
}

// const result = problem3(inventory);
// console.log(result);

module.exports = problem3;