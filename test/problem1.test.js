const problem1 = require('../problem1.cjs');
const inventory = require('../inventory.cjs');

const expectedResult = {"id":33,"car_make":"Jeep","car_model":"Wrangler","car_year":2011}
test('Problem 1', () => {
    expect(problem1(inventory, 33)).toStrictEqual(expectedResult);
});
