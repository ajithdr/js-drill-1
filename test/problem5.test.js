const problem5 = require('../problem5.cjs');
const inventory = require('../inventory.cjs');

test('Problem 5', () => {
    expect(problem5(inventory)).toBe(25);
});