const problem2 = require('../problem2.cjs');
const inventory = require('../inventory.cjs');

const expectedResult = { id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 };

test('Problem 2', () => {
    expect(problem2(inventory)).toStrictEqual(expectedResult);
});
