/*  ==== Problem #5 ====
    The car lot manager needs to find out how many cars are older than the year 2000. 
    Using the array you just obtained from the previous problem, 
    find out how many cars were made before the year 2000 and return 
    the array of older cars and log its length.
*/

const inventory = require('./inventory.cjs');

function getCarYears(inventory) {

    const carYears = [];
    for(let index = 0; index < inventory.length; index++) {
        if(inventory[index].hasOwnProperty('car_year')) {
            carYears.push(inventory[index].car_year);
        }
    }
    return carYears;
}

function problem5(inventory) {

    if(arguments.length < 1 || !Array.isArray(inventory)) {
        return 0;
    }

    if(inventory.length == 0) {
        return 0;
    }

    const years = getCarYears(inventory);
    let numOfCars = 0;

    for(let index = 0; index < years.length; index++) {
        if(years[index] < 2000) {
            numOfCars++;
        }
    }

    return numOfCars;
}

// const result = problem5(inventory);
// console.log(result);

module.exports = problem5;