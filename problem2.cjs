/* 
    ==== Problem #2 ====
    The dealer needs the information on the last car in their inventory. 
    Execute a function to find what the make and model of the last car in 
    the inventory is?  Log the make and model into the console in the format of:
    
    "Last car is a *car make goes here* *car model goes here*"
*/

const inventory = require('./inventory.cjs');

function problem2(inventory) {

    if(arguments.length < 1 || !Array.isArray(inventory)) {
        return [];
    }

    if(inventory.length == 0) {
        return [];
    }

    const lastIndex = inventory.length-1;
    return inventory[lastIndex];
}

// const result = problem2(inventory);
// console.log(result);

module.exports = problem2;